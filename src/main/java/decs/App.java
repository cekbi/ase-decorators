package decs;

import java.util.Collections;
import java.util.List;

import decs.Cow;

class App {
	
	public static void draw_stdout(Drawable drawable) {
		List<ASCEL> ascels = drawable.draw();
		Collections.sort(ascels);
		System.out.println(ascels);
		int curLine = 0;
		int curCol = 0;
		System.out.println("X: \033[2J\033[1;1H");
		for (ASCEL ascel : ascels) {
			while (curLine <= ascel.getY()) {
				System.out.println();
				System.out.print("");
				curLine++;
				curCol = 0;
			}
			// if  (curCol == ascel.getX()) {
			// 	continue;
			// }
			while (curCol < ascel.getX()) {
				System.out.print(" ");
				curCol++;
			}
			curCol = ascel.getX() + 1;
			if (ascel.getColor() == null) {
				System.out.printf("\033[0m");
			} else {
				System.out.printf("\033[38;2;%d;%d;%dm", ascel.getColor().r, ascel.getColor().g, ascel.getColor().b);
			}
			System.out.print(ascel.getC());			
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Drawable graph = new Cow();
		graph = new Say("Moo", graph);
		graph = new Say("You want some?", graph);
		graph = new Say("I give milk", graph);
		graph = new Box(graph);
		graph = new Rainbow(graph);

		draw_stdout(graph);
	}

}
